/***********************************************************************************
* System Monitor: Plasmoid and data engines to monitor CPU/Memory/Swap Usage.
* Copyright (C) 2008  Matthew Dawson
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
***********************************************************************************/

#include "system_status.h"

#include <Plasma/ToolTipManager>
#include <Plasma/ToolTipContent>

#include <KConfigDialog>

#include <QPainter>

#include <cmath>

SystemStatus::SystemStatus(QObject *parent, const QVariantList &args)
        : Plasma::Applet(parent, args),
        m_ramfree(0),
        m_ramused(0),
        m_rambuffers(0),
        m_ramcached(0),
        m_ramtotal(1),
        m_swapfree(0),
        m_swapused(0),
        m_swaptotal(1)
{

    m_cpuInfo.resize(1);
    setHasConfigurationInterface(true);
    resize(100, 100);

}
SystemStatus::~SystemStatus()
{

    if (hasFailedToLaunch()) {

    } else {

    }

}

void SystemStatus::init()
{

    readConfig();

    sys_mon = dataEngine("systemmonitor");
    connect(sys_mon, SIGNAL(sourceAdded(const QString &)), this, SLOT(sourcesAdded(const QString &)));
    reconnectSources();

    Plasma::ToolTipManager::self()->registerWidget(this);

}

void SystemStatus::reconnectSources()
{

    reconnectCPUSources();

    sys_mon->connectSource("mem/physical/used", this, 500);
    sys_mon->connectSource("mem/physical/free", this, 500);
    sys_mon->connectSource("mem/physical/buf", this, 500);
    sys_mon->connectSource("mem/physical/cached", this, 500);

    sys_mon->connectSource("mem/swap/used", this, 500);
    sys_mon->connectSource("mem/swap/free", this, 500);

}

void SystemStatus::reconnectCPUSources()
{

    if (!m_showMultiCPU) {

        sys_mon->connectSource("cpu/system/user", this, 500);
        sys_mon->connectSource("cpu/system/sys", this, 500);
        sys_mon->connectSource("cpu/system/nice", this, 500);
        sys_mon->connectSource("cpu/system/wait", this, 500);
        sys_mon->connectSource("cpu/system/idle", this, 500);

    } else {

        sys_mon->connectSource("system/cores", this, 500);

        m_numCPUs = sys_mon->query("system/cores")["value"].toUInt();
        if (m_numCPUs == 0) {
            //If we have zero, either the sources doesn't exist or theres a problem.
            return ;
        }

        m_cpuInfo.resize(m_numCPUs);

        for (uint i = 0; i < m_numCPUs; ++i) {

            sys_mon->connectSource(QString("cpu/cpu%1/user").arg(i), this, 500);
            sys_mon->connectSource(QString("cpu/cpu%1/sys").arg(i), this, 500);
            sys_mon->connectSource(QString("cpu/cpu%1/nice").arg(i), this, 500);
            sys_mon->connectSource(QString("cpu/cpu%1/wait").arg(i), this, 500);
            sys_mon->connectSource(QString("cpu/cpu%1/idle").arg(i), this, 500);

        }

    }

}

void SystemStatus::disconnectSources()
{

    disconnectCPUSources();

    sys_mon->disconnectSource("mem/physical/used", this);
    sys_mon->disconnectSource("mem/physical/free", this);
    sys_mon->disconnectSource("mem/physical/buf", this);
    sys_mon->disconnectSource("mem/physical/cached", this);

    sys_mon->disconnectSource("mem/swap/used", this);
    sys_mon->disconnectSource("mem/swap/free", this);

}

void SystemStatus::disconnectCPUSources()
{

    if (!m_showMultiCPU) {

        sys_mon->disconnectSource("cpu/system/user", this);
        sys_mon->disconnectSource("cpu/system/sys", this);
        sys_mon->disconnectSource("cpu/system/nice", this);
        sys_mon->disconnectSource("cpu/system/wait", this);
        sys_mon->disconnectSource("cpu/system/idle", this);

    } else {

        for (uint i = 0; i < m_numCPUs; ++i) {

            sys_mon->disconnectSource(QString("cpu/cpu%1/user").arg(i), this);
            sys_mon->disconnectSource(QString("cpu/cpu%1/sys").arg(i), this);
            sys_mon->disconnectSource(QString("cpu/cpu%1/nice").arg(i), this);
            sys_mon->disconnectSource(QString("cpu/cpu%1/wait").arg(i), this);
            sys_mon->disconnectSource(QString("cpu/cpu%1/idle").arg(i), this);

        }

    }

}

void SystemStatus::sourcesAdded(const QString &source)
{

    if (source == "system/cores") {

        reconnectCPUSources();

    } else if (source.startsWith("cpu/system/") && !m_showMultiCPU) {

        if (source.endsWith("/user") || source.endsWith("/sys") || source.endsWith("/nice") || source.endsWith("/wait") || source.endsWith("/idle")) {

            sys_mon->connectSource(source, this, 500);

        }

    } else if (source.startsWith("cpu/cpu") && m_showMultiCPU) {

        if (source.endsWith("/user") || source.endsWith("/sys") || source.endsWith("/nice") || source.endsWith("/wait") || source.endsWith("/idle")) {

            sys_mon->connectSource(source, this, 500);

        }

    } else if (source.startsWith("mem/swap")) {

        sys_mon->connectSource(source, this, 500);

    } else if (source.startsWith("mem/physical/")) {

        if (!source.endsWith("application")) {

            sys_mon->connectSource(source, this, 500);

        }

    }

}

void SystemStatus::constraintsEvent(Plasma::Constraints constraints)
{

    if (constraints.testFlag(Plasma::FormFactorConstraint)) {

        if (formFactor() == Plasma::Planar || formFactor() == Plasma::MediaCenter) {
            setAspectRatioMode(Plasma::IgnoreAspectRatio);
        } else {
            setAspectRatioMode(Plasma::ConstrainedSquare);
        }

        //If only StandardBackground is set, it should be completely transparent. (it gets
        //set in Plasma::Applet on certain conditions.
        if (backgroundHints() == StandardBackground) {
            setBackgroundHints(NoBackground);
        }

    }

}

void SystemStatus::createConfigurationInterface(KConfigDialog *parent)
{

    QWidget *widGeneral = new QWidget();
    uiGeneral.setupUi(widGeneral);

    uiGeneral.chkIsVertical->setChecked(m_isVertical);
    uiGeneral.chkUseOxygen->setChecked(m_useOxygen);
    uiGeneral.kcmbBackground->setItemData(0, DefaultBackground);
    uiGeneral.kcmbBackground->setItemData(1, TranslucentBackground);
    uiGeneral.kcmbBackground->setItemData(2, NoBackground);

    switch ((int)backgroundHints()) {

    case(int)DefaultBackground:
        uiGeneral.kcmbBackground->setCurrentIndex(0);
        break;
        //StandardBackground may get set.  Doesn't effect drawing, so this just makes sure we catch it.
    case(int)TranslucentBackground:
    case(int)TranslucentBackground | StandardBackground:
        uiGeneral.kcmbBackground->setCurrentIndex(1);
        break;
    case(int)NoBackground:
        uiGeneral.kcmbBackground->setCurrentIndex(2);
        break;
    default:
        Q_ASSERT("Unknown background hints!");
        break;

    }

    QWidget *widColours = new QWidget();
    uiColours.setupUi(widColours);

    uiColours.kcbCpuUser->setColor(this->m_cpuUserColour);
    uiColours.kcbCpuNice->setColor(this->m_cpuNiceColour);
    uiColours.kcbCpuDisk->setColor(this->m_cpuDiskColour);
    uiColours.kcbCpuSystem->setColor(this->m_cpuSysColour);
    uiColours.kcbRamBuffers->setColor(this->m_ramBuffersColour);
    uiColours.kcbRamCached->setColor(this->m_ramCachedColour);
    uiColours.kcbRamUser->setColor(this->m_ramUsedColour);
    uiColours.kcbSwap->setColor(this->m_swapUsedColour);

    parent->setButtons(KDialog::Ok | KDialog::Cancel | KDialog::Apply);
    connect(parent, SIGNAL(applyClicked()), this, SLOT(configUpdated()));
    connect(parent, SIGNAL(okClicked()), this, SLOT(configUpdated()));

    parent->addPage(widGeneral, "General", icon(), QString(), false);
    parent->addPage(widColours, "Colours", icon(), QString(), false);

    parent->setFaceType(KConfigDialog::Tabbed);

}

void SystemStatus::configUpdated()
{

    KConfigGroup cg = config();

    if (uiGeneral.kcmbBackground->itemData(uiGeneral.kcmbBackground->currentIndex()).toInt() != (int)this->backgroundHints()) {
        this->setBackgroundHints(QFlag(uiGeneral.kcmbBackground->itemData(uiGeneral.kcmbBackground->currentIndex()).toInt()));
        cg.writeEntry("background", (int)this->backgroundHints());
    }

    if (uiGeneral.chkIsVertical->isChecked() != this->m_isVertical) {
        this->m_isVertical = uiGeneral.chkIsVertical->isChecked();
        cg.writeEntry("vertical", this->m_isVertical);
    }
    if (uiGeneral.chkUseOxygen->isChecked() != this->m_useOxygen) {
        this->m_useOxygen = uiGeneral.chkUseOxygen->isChecked();
        cg.writeEntry("use_oxygen", this->m_useOxygen);
    }
    if (uiColours.kcbCpuUser->color() != this->m_cpuUserColour) {
        this->m_cpuUserColour = uiColours.kcbCpuUser->color();
        cg.writeEntry("colour_cpu_user", this->m_cpuUserColour.name());
    }
    if (uiColours.kcbCpuNice->color() != this->m_cpuNiceColour) {
        this->m_cpuNiceColour = uiColours.kcbCpuNice->color();
        cg.writeEntry("colour_cpu_nice", this->m_cpuNiceColour.name());
    }
    if (uiColours.kcbCpuDisk->color() != this->m_cpuDiskColour) {
        this->m_cpuDiskColour = uiColours.kcbCpuDisk->color();
        cg.writeEntry("colour_cpu_disk", this->m_cpuDiskColour.name());
    }
    if (uiColours.kcbCpuSystem->color() != this->m_cpuSysColour) {
        this->m_cpuSysColour = uiColours.kcbCpuSystem->color();
        cg.writeEntry("colour_cpu_sys", this->m_cpuSysColour.name());
    }
    if (uiColours.kcbRamCached->color() != this->m_ramCachedColour) {
        this->m_ramCachedColour = uiColours.kcbRamCached->color();
        cg.writeEntry("colour_ram_cached", this->m_ramCachedColour.name());
    }
    if (uiColours.kcbRamBuffers->color() != this->m_ramBuffersColour) {
        this->m_ramBuffersColour = uiColours.kcbRamBuffers->color();
        cg.writeEntry("colour_ram_buffers", this->m_ramBuffersColour.name());
    }
    if (uiColours.kcbRamUser->color() != this->m_ramUsedColour) {
        this->m_ramUsedColour = uiColours.kcbRamUser->color();
        cg.writeEntry("colour_ram_used", this->m_ramUsedColour.name());
    }
    if (uiColours.kcbSwap->color() != this->m_swapUsedColour) {
        this->m_swapUsedColour = uiColours.kcbSwap->color();
        cg.writeEntry("colour_swap_used", this->m_swapUsedColour.name());
    }

    emit configNeedsSaving();
    update();

}

void SystemStatus::dataUpdated(const QString& source, const Plasma::DataEngine::Data &data)
{

    if (source == "system/cores" && m_showMultiCPU) {

        if (data["value"].toUInt() != m_numCPUs) {
            reconnectCPUSources();
        }

    } else if (source.startsWith("cpu/system/") && !m_showMultiCPU) {

        if (source.endsWith("/user")) {

            m_cpuInfo[0].user = (data["value"].toString().toDouble()) / 100;

        } else if (source.endsWith("/sys")) {

            m_cpuInfo[0].sys = (data["value"].toString().toDouble()) / 100;

        } else if (source.endsWith("/nice")) {

            m_cpuInfo[0].nice = (data["value"].toString().toDouble()) / 100;

        } else if (source.endsWith("/wait")) {

            m_cpuInfo[0].disk = (data["value"].toString().toDouble()) / 100;

        } else if (source.endsWith("/idle")) {

            m_cpuInfo[0].idle = (data["value"].toString().toDouble()) / 100;

        }

    } else if (source.startsWith("cpu/") && m_showMultiCPU) {

        int cpu = source.split('/')[1].mid(3).toInt();

        if (cpu >= m_cpuInfo.size()) {
            Q_ASSERT("CPU is not supposed to exist!");
            return ;
        }

        if (source.endsWith("/user")) {

            m_cpuInfo[cpu].user = (data["value"].toString().toDouble()) / 100;

        } else if (source.endsWith("/sys")) {

            m_cpuInfo[cpu].sys = (data["value"].toString().toDouble()) / 100;

        } else if (source.endsWith("/nice")) {

            m_cpuInfo[cpu].nice = (data["value"].toString().toDouble()) / 100;

        } else if (source.endsWith("/wait")) {

            m_cpuInfo[cpu].disk = (data["value"].toString().toDouble()) / 100;

        } else if (source.endsWith("/idle")) {

            m_cpuInfo[cpu].idle = (data["value"].toString().toDouble()) / 100;

        }

    } else if (source.startsWith("mem/swap")) {

        if (source.endsWith("/used")) {

            m_swapused = data["value"].toDouble() / 100;

        } else if (source.endsWith("/free")) {

            m_swapfree = data["value"].toDouble() / 100;

        }

        m_swaptotal = m_swapfree + m_swapused;

        if (m_swaptotal == 0) {

            m_swaptotal = 1;

        }

        update();

    } else if (source.startsWith("mem/physical/")) {

        if (source.endsWith("/used")) {

            m_ramused = data["value"].toDouble();

        } else if (source.endsWith("/cached")) {

            m_ramcached = data["value"].toDouble();

        } else if (source.endsWith("/buf")) {

            m_rambuffers = data["value"].toDouble();

        } else if (source.endsWith("/free")) {

            m_ramfree = data["value"].toDouble();

        }

        m_ramtotal = m_ramused + m_ramcached + m_rambuffers + m_ramfree;

        if (m_ramtotal == 0) {

            m_ramtotal = 1;

        }

    }

}

void SystemStatus::paintCPUUsage(QPainter *p, const QStyleOptionGraphicsItem *option, const QRect& contentsRect, const cpuInfo &cpu)
{

    Q_UNUSED(option)

    p->save();

    p->translate(0, contentsRect.height() * cpu.idle);


    p->setBrush(this->m_cpuUserColour);
    p->setPen(this->m_cpuUserColour);

    p->drawRect(QRectF(contentsRect.left(), contentsRect.top(), contentsRect.width(), contentsRect.height() * cpu.user));
    p->translate(0, contentsRect.height() * cpu.user);


    p->setBrush(this->m_cpuNiceColour);
    p->setPen(this->m_cpuNiceColour);

    p->drawRect(QRectF(contentsRect.left(), contentsRect.top(), contentsRect.width(), contentsRect.height() * cpu.nice));
    p->translate(0, contentsRect.height() * cpu.nice);


    p->setBrush(this->m_cpuDiskColour);
    p->setPen(this->m_cpuDiskColour);

    p->drawRect(QRectF(contentsRect.left(), contentsRect.top(), contentsRect.width(), contentsRect.height() * cpu.disk));
    p->translate(0, contentsRect.height() * cpu.disk);


    p->setBrush(this->m_cpuSysColour);
    p->setPen(this->m_cpuSysColour);

    p->drawRect(QRectF(contentsRect.left(), contentsRect.top(), contentsRect.width(), contentsRect.height() * cpu.sys));

    p->restore();

}

void SystemStatus::paintSwapUsage(QPainter *p, const QStyleOptionGraphicsItem *option, const QRect& contentsRect)
{

    Q_UNUSED(option)

    p->save();

    p->translate(0, contentsRect.height() * m_swapfree / m_swaptotal);


    p->setBrush(this->m_swapUsedColour);
    p->setPen(this->m_swapUsedColour);

    p->drawRect(QRectF(contentsRect.left(), contentsRect.top(), contentsRect.width(), contentsRect.height() * m_swapused / m_swaptotal));

    p->restore();

}

void SystemStatus::paintRAMUsage(QPainter *p, const QStyleOptionGraphicsItem *option, const QRect& contentsRect)
{

    Q_UNUSED(option)

    p->save();

    p->translate(0, contentsRect.height() * m_ramfree / m_ramtotal);


    p->setBrush(this->m_ramCachedColour);
    p->setPen(this->m_ramCachedColour);

    p->drawRect(QRectF(contentsRect.left(), contentsRect.top(), contentsRect.width(), contentsRect.height() * m_ramcached / m_ramtotal));
    p->translate(0, contentsRect.height() * m_ramcached / m_ramtotal);


    p->setBrush(this->m_ramBuffersColour);
    p->setPen(this->m_ramBuffersColour);

    p->drawRect(QRectF(contentsRect.left(), contentsRect.top(), contentsRect.width(), contentsRect.height() * m_rambuffers / m_ramtotal));
    p->translate(0, contentsRect.height() * m_rambuffers / m_ramtotal);


    p->setBrush(this->m_ramUsedColour);
    p->setPen(this->m_ramUsedColour);

    p->drawRect(QRectF(contentsRect.left(), contentsRect.top(), contentsRect.width(), contentsRect.height() * m_ramused / m_ramtotal));

    p->restore();

}

void SystemStatus::paintFrontGlass(QPainter* p, const QStyleOptionGraphicsItem *option, const QRect& contentsRect)
{

    Q_UNUSED(option);

    QLinearGradient grad(0, contentsRect.top(), 0, contentsRect.bottom());
    grad.setColorAt(0, QColor(255, 255, 255, 30));
    grad.setColorAt(1, QColor(0,  0,  0, 150));
    p->setBrush(grad);
    p->setPen(QColor(0, 0, 0, 100));
    p->drawRect(contentsRect);

}

void SystemStatus::paintInterface(QPainter *p, const QStyleOptionGraphicsItem *option, const QRect& contentsRect)
{

    p->setRenderHint(QPainter::SmoothPixmapTransform);
    p->setRenderHint(QPainter::Antialiasing);

    QRect rotatedContentsRect(contentsRect);


    if (!this->m_isVertical && (this->formFactor() == Plasma::Horizontal || this->formFactor() == Plasma::Vertical)) {
        p->setWorldTransform(QTransform().rotate(90).translate(0, -(contentsRect.width() + contentsRect.x() * 2)));
        rotatedContentsRect.setWidth(contentsRect.height());
        rotatedContentsRect.setHeight(contentsRect.width());
    }

    int origWidth = rotatedContentsRect.width();

    double widthDivisor;
    if (m_showMultiCPU) {

        widthDivisor = 1.0 / (double)(m_numCPUs + 2);

    } else {

        widthDivisor = 0.33;

    }

    if (m_useOxygen) {
        rotatedContentsRect.setWidth(rotatedContentsRect.width() * (widthDivisor - 0.005));
        rotatedContentsRect.moveLeft(rotatedContentsRect.left() + origWidth * 0.009);
    } else {
        rotatedContentsRect.setWidth(rotatedContentsRect.width() * widthDivisor);
    }

    if (!m_showMultiCPU) {
        paintCPUUsage(p, option, rotatedContentsRect, m_cpuInfo[0]);
        if (m_useOxygen) {
            paintFrontGlass(p, option, rotatedContentsRect);
        }
        rotatedContentsRect.moveLeft(rotatedContentsRect.left() + widthDivisor*origWidth);
    } else {

        for (uint i = 0; i < m_numCPUs;++i) {

            paintCPUUsage(p, option, rotatedContentsRect, m_cpuInfo[i]);
            if (m_useOxygen) {
                paintFrontGlass(p, option, rotatedContentsRect);
            }
            rotatedContentsRect.moveLeft(rotatedContentsRect.left() + widthDivisor*origWidth);

        }


    }

    paintRAMUsage(p, option, rotatedContentsRect);
    if (m_useOxygen) {
        paintFrontGlass(p, option, rotatedContentsRect);
    }
    rotatedContentsRect.moveLeft(rotatedContentsRect.left() + widthDivisor*origWidth);

    paintSwapUsage(p, option, rotatedContentsRect);
    if (m_useOxygen) {
        paintFrontGlass(p, option, rotatedContentsRect);
    }


}

void SystemStatus::readConfig()
{

    KConfigGroup cg = config();

    this->m_isVertical = cg.readEntry("vertical", true);
    this->m_useOxygen = cg.readEntry("use_oxygen", false);
    this->m_showMultiCPU = cg.readEntry("show_multiple_cpus", false);

    this->m_cpuUserColour = QColor(cg.readEntry("colour_cpu_user", QString("#0000FF")));
    this->m_cpuNiceColour = QColor(cg.readEntry("colour_cpu_nice", QString("#FFFF00")));
    this->m_cpuDiskColour = QColor(cg.readEntry("colour_cpu_disk", QString("#006400")));
    this->m_cpuSysColour = QColor(cg.readEntry("colour_cpu_sys", QString("#FF0000")));
    this->m_ramCachedColour = QColor(cg.readEntry("colour_ram_cached", QString("#007800")));
    this->m_ramBuffersColour = QColor(cg.readEntry("colour_ram_buffers", QString("#FFFF00")));
    this->m_ramUsedColour = QColor(cg.readEntry("colour_ram_used", QString("#0000B1")));
    this->m_swapUsedColour = QColor(cg.readEntry("colour_swap_used", QString("#00CDCD")));

    this->setBackgroundHints(QFlag(cg.readEntry("background", (int)(TranslucentBackground))));

}

QList<QAction*> SystemStatus::contextualActions()
{

    QAction *swapMultiCPUShow = new QAction("Show multiple CPUs Status", this);

    swapMultiCPUShow->setCheckable(true);
    swapMultiCPUShow->setChecked(m_showMultiCPU);

    connect(swapMultiCPUShow, SIGNAL(toggled(bool)), this, SLOT(swapShowingMultiCPU()));

    return QList<QAction *>() << swapMultiCPUShow;

}

void SystemStatus::swapShowingMultiCPU()
{

    disconnectCPUSources();

    m_showMultiCPU = !m_showMultiCPU;
    config().writeEntry("show_multiple_cpus", m_showMultiCPU);

    reconnectCPUSources();

}

void SystemStatus::toolTipAboutToShow()
{

    Plasma::ToolTipManager::self()->setContent(this, Plasma::ToolTipContent("System Status:", QString("CPU usage: %1%<br>Ram Usage: %2%<br>Swap Usage: %3%").arg(round((1 - m_cpuInfo[0].idle) * 100)).arg(round((1 - m_ramfree / m_ramtotal) * 100)).arg(round((1 - m_swapfree / m_swaptotal) * 100))));

}

#include "system_status.moc"
