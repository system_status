/***********************************************************************************
* System Monitor: Plasmoid and data engines to monitor CPU/Memory/Swap Usage.
* Copyright (C) 2008  Matthew Dawson
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
***********************************************************************************/

#ifndef SYSTEM_MONITOR_H
#define SYSTEM_MONITOR_H

#include <Plasma/Applet>
#include <Plasma/DataEngine>

#include <QVector>

#include "ui_coloursConfig.h"
#include "ui_generalConfig.h"

class QString;
class QSizeF;
class QPainter;
class QStyleOptionGraphicsItem;

struct cpuInfo {

    double user, sys, nice, idle, disk;
    cpuInfo(): user(0), sys(0), nice(0), idle(0), disk(0) {}

};

class SystemStatus : public Plasma::Applet
{
    Q_OBJECT

public:
    SystemStatus(QObject *parent, const QVariantList &args);
    ~SystemStatus();

    void constraintsEvent(Plasma::Constraints constraints);

    void paintInterface(QPainter *painter, const QStyleOptionGraphicsItem *option, const QRect& contentsRect);
    void init();

    QList<QAction*> contextualActions();

protected slots:
    void dataUpdated(const QString& source, const Plasma::DataEngine::Data &data);
    void configUpdated();
    void sourcesAdded(const QString& source);
    void toolTipAboutToShow();

    void swapShowingMultiCPU();

private:
    void paintCPUUsage(QPainter *p, const QStyleOptionGraphicsItem *option, const QRect& contentsRect, const cpuInfo &cpu);
    void paintSwapUsage(QPainter *p, const QStyleOptionGraphicsItem *option, const QRect& contentsRect);
    void paintRAMUsage(QPainter *p, const QStyleOptionGraphicsItem *option, const QRect& contentsRect);
    void paintFrontGlass(QPainter *p, const QStyleOptionGraphicsItem *option, const QRect& contentsRect);

    void readConfig();
    void createConfigurationInterface(KConfigDialog *parent);

    void reconnectSources();
    void reconnectCPUSources();
    void disconnectSources();
    void disconnectCPUSources();

    QVector<cpuInfo> m_cpuInfo;
    uint m_numCPUs;
    double m_ramfree;
    double m_ramused;
    double m_rambuffers;
    double m_ramcached;
    double m_ramtotal;
    double m_swapfree;
    double m_swapused;
    double m_swaptotal;

    Ui::generalConfig uiGeneral;
    Ui::coloursConfig uiColours;

    bool m_isVertical;
    bool m_useOxygen;
    bool m_showMultiCPU;
    QColor m_cpuUserColour;
    QColor m_cpuNiceColour;
    QColor m_cpuDiskColour;
    QColor m_cpuSysColour;
    QColor m_ramCachedColour;
    QColor m_ramBuffersColour;
    QColor m_ramUsedColour;
    QColor m_swapUsedColour;

    Plasma::DataEngine *sys_mon;
};

K_EXPORT_PLASMA_APPLET(system_status, SystemStatus)

#endif
